from fastapi import FastAPI, status, HTTPException, Depends
from geopy.distance import geodesic
from sqlalchemy.orm import Session
import logging

from database import Base, engine, SessionLocal
import models
import schemas

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

f = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
fh = logging.FileHandler('server_logs.log')
fh.setFormatter(f)

logger.addHandler(fh)
logger.info('Start main script')


def get_session():
    session = SessionLocal()
    logger.info('Connecting to database')
    try:
        yield session
    finally:
        session.close()


Base.metadata.create_all(engine)

app = FastAPI()


# Get all addresses
@app.get("/all-address")
def all_address(session: Session = Depends(get_session)):
    addressdb = session.query(models.AddressDb).all()
    logger.info('Getting all addresses from database')
    session.close()
    return addressdb


# Retrive addresses by coordinates within a given distance
@app.get("/get-address-by-distance-and-coordinates")
def get_address_by_distance_and_coordinates(distance: float, latitude: float, longitude: float, session: Session = Depends(get_session)):
    '''
    This endpoint accepts 3 query parameters 
    i.e. Distance, Latitude and Longitude
    and returns all those addresses that are 
    within the specified distance from the specified coordinates.  

    '''
    addressdb = session.query(models.AddressDb).all()
    logger.info(f'Finding the addresses from {latitude} deg and {longitude} deg withing {distance} kms')

    addresses = []
    for i in range(len(addressdb)):
        coordinate = (addressdb[i].latitude, addressdb[i].longitude)
        dist = geodesic(coordinate, (latitude, longitude)).km
        if round(dist, 2) <= distance:
            addresses.append(addressdb[i])

    session.close()

    if not addresses:
        raise HTTPException(
            status_code=404, detail=f"Addresses within a distance of {distance} kms is not present from coordinates {latitude} and {longitude}")
    else:
        logger.info('Retreived the addresses successfully')

    return addresses


# Create a new address
@app.post("/create-address", status_code=status.HTTP_201_CREATED)
def create_address(add: schemas.AddressSchema, session: Session = Depends(get_session)):
    logger.info('Adding address')
    logger.info(
        f'Name:"{add.name}" from city: "{add.address}"')

    addressdb = models.AddressDb(
        name=add.name,
        phone_no=add.phone_no,
        address=add.address,
        latitude=add.lat,
        longitude=add.long
    )

    session.add(addressdb)
    session.commit()
    id = addressdb.id
    logger.info('Added address to database table')
    session.close()

    return f"Created address with id {id}"


# Update an address with id
@app.put("/update-address/{id}")
def update_address(id: int, add: schemas.AddressUpdateSchema, session: Session = Depends(get_session)):
    addressdb = session.query(models.AddressDb).get(id)
    logger.info(f'Updating the address of id: {id} ')

    if addressdb:
        addressdb.name = add.name
        addressdb.phone_no = add.phone_no
        addressdb.address = add.address
        addressdb.latitude = add.lat
        addressdb.longitude = add.long
        session.commit()

    session.close()

    if not addressdb:
        raise HTTPException(
            status_code=404, detail=f"Address with id {id} not found")
    else:
        logger.info('Updated successfully')
    return addressdb


# Delete an address with id
@app.delete("/delete-address/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_address(id: int, session: Session = Depends(get_session)):
    addressdb = session.query(models.AddressDb).get(id)
    logger.info(f'Deleting the address with id: {id}')
    
    if addressdb:
        session.delete(addressdb)
        session.commit()
        session.close()
    else:
        raise HTTPException(
            status_code=404, detail=f"Address with id {id} not found")

    logger.info('Deleted successfully')
    return None
