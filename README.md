# EastVantage assignment

### Steps to run the project
1. Clone the repository
2. Navigate to the cloned directory and download the dependencies with `pip install -r requirements.txt`
3. Run the project with `uvicorn main:app --reload`
4. Navigate to `http://127.0.0.1:8000/docs#/` from your browser

### Endpoints
1. `/all-address` : Retrieve all the addresses in the Database
2. `/get-address-by-distance-and-coordinates` : Retrieve all those addresses within a specified distance from the coordinates
3. `/create-address` : Create an address
4. `/update-address/{id}` : Update an address with id
5. `/delete-address/{id}` : Delete an address with id
