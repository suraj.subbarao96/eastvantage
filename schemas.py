from pydantic import BaseModel
from typing import Optional


class AddressSchema(BaseModel):
    name: str
    phone_no: int
    address: str
    lat: float
    long: float


class AddressUpdateSchema(BaseModel):
    name: Optional[str] = None
    phone_no: Optional[int] = None
    address: Optional[str] = None
    lat: Optional[float] = None
    long: Optional[float] = None