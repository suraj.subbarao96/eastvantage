from sqlalchemy import Column, Integer, String, Float
from database import Base


class AddressDb(Base):
    __tablename__ = 'address'
    id = Column(Integer, primary_key=True)
    name = Column(String(256))
    phone_no = Column(Integer)
    address = Column(String(256))
    latitude = Column(Float)
    longitude = Column(Float)